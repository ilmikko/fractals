package grid

import (
	"../point"
	"../viewport"
)

type Grid struct {
	Width     int
	Height    int
	Unescaped []*Cell

	cells map[int]map[int]*Cell
}

type Cell struct {
	X       int
	Y       int
	Value   *point.Point
	Pos     *point.Point
	Escaped bool
}

func New(w, h int) *Grid {
	return &Grid{
		Width:     w,
		Height:    h,
		Unescaped: []*Cell{},

		cells: map[int]map[int]*Cell{},
	}
}

func (g *Grid) Cell(x, y int) *Cell {
	if _, ok := g.cells[y]; !ok {
		g.cells[y] = map[int]*Cell{}
	}

	if _, ok := g.cells[y][x]; !ok {
		g.cells[y][x] = &Cell{}
	}

	return g.cells[y][x]
}

func (g *Grid) SetViewport(vp *viewport.Viewport) {
	g.Unescaped = nil
	g.Unescaped = []*Cell{}

	for gx := 0; gx < g.Width; gx++ {
		for gy := 0; gy < g.Height; gy++ {
			c := g.Cell(gx, gy)

			p := vp.Transform(
				float64(gx),
				float64(gy),
				float64(g.Width),
				float64(g.Height),
			)

			c.X = gx
			c.Y = gy

			c.Pos = p
			c.Value = p
			c.Escaped = false

			g.Unescaped = append(g.Unescaped, c)
		}
	}
}
