package point

import ()

type Point struct {
	X float64
	Y float64
}

func New(x, y float64) *Point {
	return &Point{
		X: x,
		Y: y,
	}
}

func Clone(p *Point) *Point {
	return &Point{
		X: p.X,
		Y: p.Y,
	}
}
