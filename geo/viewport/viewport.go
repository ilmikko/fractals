package viewport

import (
	"fmt"

	"../point"
)

type Viewport struct {
	X      float64
	Y      float64
	Width  float64
	Height float64
}

func Default() *Viewport {
	return &Viewport{
		X:      0,
		Y:      0,
		Width:  4,
		Height: 4,
	}
}

func String(vp *Viewport) string {
	return fmt.Sprintf(
		"x:%v.y:%v.w:%v.h:%v",
		vp.X,
		vp.Y,
		vp.Width,
		vp.Height,
	)
}

func (v *Viewport) Transform(x, y, w, h float64) *point.Point {
	// Convert pixel x and y coordinates to grid x and y values.
	px := x / w
	py := y / h

	vx := v.X - v.Width/2 + px*v.Width
	vy := v.Y - v.Height/2 + py*v.Height

	return point.New(vx, vy)
}

func (v *Viewport) MoveTo(x, y float64) {
	v.X += -v.Width/2 + x*v.Width
	v.Y += -v.Height/2 + y*v.Height
}

func (v *Viewport) ZoomBy(z float64) {
	v.Width /= z
	v.Height /= z
}
