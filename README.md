# fractals

Fractal explorer and natural wallpaper generator written in Go.

Zooms into a random point of any fractal.
By default uses the Mandelbrot set, but can have any escape set to visualize.
You can fine-tune parameters such as color palette, resolution and how many iterations to calculate until moving to the next picture by "zooming in" to a random point of the fractal.

## Examples

![Mandelbrot fractal](img/fractals4.png)

![Mandelbrot fractal](img/fractals5.png)

![Mandelbrot fractal](img/fractalsb8.png)

![Mandelbrot fractal](img/fractals6.png)

![Mandelbrot fractal](img/fractalsb2.png)

![Mandelbrot fractal](img/fractalsb4.png)

![Mandelbrot fractal](img/fractalsb5.png)

![Mandelbrot fractal](img/fractalsb7.png)

![Mandelbrot fractal](img/fractals7.png)

![Mandelbrot fractal](img/fractalsb1.png)

![Mandelbrot fractal](img/fractals8.png)

![Mandelbrot fractal](img/fractals.png)

![Mandelbrot fractal](img/fractalsb10.png)

![Mandelbrot fractal](img/fractalsb11.png)

![Mandelbrot fractal](img/fractalsb12.png)

![Mandelbrot fractal](img/fractalsb13.png)
