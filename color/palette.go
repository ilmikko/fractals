package color

import (
	"math"
)

type Palette []*Color

var (
	PALETTE_BW = Palette{
		New(0, 0, 0),
		New(1, 1, 1),
	}
	PALETTE_RAINBOW = Palette{
		New(0, 0, 0),
		New(1, 0, 0),
		New(1, 1, 0),
		New(0, 1, 0),
		New(0, 1, 1),
		New(0, 0, 1),
		New(1, 0, 1),
		New(1, 1, 1),
		New(0, 0, 0),
	}
	PALETTE_RED = Palette{
		New(0, 0, 0),
		New(0.5, 0, 0),
		New(1, 0, 0.3),
		New(0.3, 0, 1),
		New(0, 0, 1),
		New(0, 0, 0),
	}
	PALETTE_GREEN = Palette{
		New(0, 0, 0),
		New(0, 1, 0),
		New(1, 1, 0),
		New(1, 0, 1),
		New(1, 1, 1),
		New(0, 1, 1),
	}
	PALETTE_BLUE = Palette{
		New(0, 0, 0),
		New(0, 0, 1),
		New(0, 1, 1),
		New(1, 0, 1),
		New(1, 0, 0),
		New(0, 0, 0),
	}
	PALETTE_SEA = Palette{
		NewHex("99d5c9"),
		NewHex("6c969d"),
		NewHex("645e9d"),
		NewHex("392b58"),
		NewHex("2d0320"),
	}
)

func (p Palette) GetColor(f float64) *Color {
	_, f = math.Modf(f)
	// Scale to the size of our palette
	f *= float64(len(p) - 1)
	index := int(math.Floor(f))
	return Lerp(p[index], p[index+1], f)
}
