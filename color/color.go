package color

import (
	"encoding/hex"
	"math"
)

type Color struct {
	R float64
	G float64
	B float64
}

func Lerp(c1, c2 *Color, f float64) *Color {
	_, f = math.Modf(f)
	return &Color{
		R: c1.R + (c2.R-c1.R)*f,
		G: c1.G + (c2.G-c1.G)*f,
		B: c1.B + (c2.B-c1.B)*f,
	}
}

func New(r, g, b float64) *Color {
	return &Color{
		R: r,
		G: g,
		B: b,
	}
}

func decodeHex(h string) float64 {
	d, err := hex.DecodeString(h)
	if err != nil {
		return 0.0
	}

	i := int(d[0])
	return float64(i) / 255.0
}

func NewHex(hex string) *Color {
	r := decodeHex(hex[0:2])
	g := decodeHex(hex[2:4])
	b := decodeHex(hex[4:6])
	return New(r, g, b)
}

func FromIteration(i int, p Palette) *Color {
	return p.GetColor(
		float64(i) / (float64(len(p)) * 256),
	)
}
