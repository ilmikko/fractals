package main

import (
	"fmt"
	"log"
	"math"
	"math/rand"
	"os"
	"time"

	"github.com/fogleman/gg"

	"./color"
	"./fractal/mandelbrot"
	"./geo/grid"
	"./geo/viewport"
)

var (
	GRID              = grid.New(512, 512)
	PALETTE           = color.PALETTE_RAINBOW
	ESCAPE_THRESHOLD  = 30
	ESCAPE_PERCENTAGE = 0.1
	ZOOM_FACTOR       = 30.0
)

func renderDebugCross(ctx *gg.Context, x, y int) {
	ctx.SetRGB(1, 0, 1)

	length := 50

	for i := -length / 2; i < length/2; i++ {
		ctx.SetPixel(x-i, y)
		ctx.SetPixel(x, y-i)
	}
}

func renderFractal(ctx *gg.Context, vp *viewport.Viewport) (nextCell *grid.Cell) {
	fractal := mandelbrot.Mandelbrot{}
	ctx.SetRGB(1, 1, 1)
	ctx.Clear()

	log.Printf("Set viewport")
	GRID.SetViewport(vp)
	log.Printf("Viewport set!")

	escaped := []*grid.Cell{}
	size := GRID.Width * GRID.Height

	for i := 0; ; i++ {
		c := color.FromIteration(i, PALETTE)

		// How many escaped this iteration?
		// This is used to cut off rendering when escape count becomes low.
		iterEscaped := 0

		unescaped := []*grid.Cell{}

		for _, cell := range GRID.Unescaped {
			if fractal.Contains(cell.Value) {
				// Did not escape yet
				cell.Value = fractal.Iterate(cell.Value, cell.Pos)
				unescaped = append(unescaped, cell)
				continue
			}

			// Escaped the set during this iteration
			ctx.SetRGB(c.R, c.G, c.B)
			ctx.SetPixel(cell.X, cell.Y)
			escaped = append(escaped, cell)
			iterEscaped += 1
		}

		{
			ue := len(GRID.Unescaped)
			e := len(escaped)
			log.Printf(".%d/%d/%d: %f", ue, e, iterEscaped, float64(ue)/float64(size))
		}

		if len(escaped) > int(ESCAPE_PERCENTAGE*float64(size)) {
			if iterEscaped < ESCAPE_THRESHOLD {
				log.Printf("Breaking at iteration %v with %v escapes (%v total)",
					i,
					iterEscaped,
					len(escaped),
				)
				break
			}
		}
		GRID.Unescaped = unescaped
	}

	// Pick a random cell from escaped set as the next zoom point.
	// We weigh this random pick by ESCAPE_THRESHOLD - as this slice is populated, the last few elements
	// are from the last few iterations.
	// If for some reason some iterations had no escapes whatsoever, we can still pick a cell from a
	// previous iteration.
	if len(escaped) > 0 {
		variance := int(
			math.Min(
				float64(ESCAPE_THRESHOLD),
				float64(len(escaped)),
			) * rand.Float64(),
		)
		nextCell = escaped[len(escaped)-1-variance]
	}

	// TODO: Handle when nextCell is nil
	return
}

func main() {
	seed := time.Now().UTC().UnixNano()
	rand.Seed(seed)

	ctx := gg.NewContext(
		GRID.Width,
		GRID.Height,
	)

	vp := viewport.Default()

	for image := 0; image < 100; image++ {
		cell := renderFractal(ctx, vp)

		// renderDebugCross(ctx, cell.X, cell.Y)

		x := float64(cell.X) / float64(GRID.Width)
		y := float64(cell.Y) / float64(GRID.Height)

		vp.MoveTo(x, y)
		vp.ZoomBy(ZOOM_FACTOR)

		os.Mkdir(
			fmt.Sprintf("./gfx/%v",
				seed,
			),
			0755,
		)

		ctx.SavePNG(
			fmt.Sprintf("./gfx/%v/%v-%s.png",
				seed,
				image,
				viewport.String(vp),
			),
		)
	}
}
