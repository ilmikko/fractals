package mandelbrot

import (
	"../../geo/point"
)

type Mandelbrot struct{}

func (m *Mandelbrot) Contains(p *point.Point) bool {
	x := p.X
	y := p.Y

	return x*x+y*y < 4
}

func (m *Mandelbrot) Iterate(p, pos *point.Point) *point.Point {
	x := p.X
	y := p.Y

	xtemp := x*x - y*y + pos.X
	y = 2*x*y + pos.Y
	x = xtemp

	return point.New(x, y)
}
